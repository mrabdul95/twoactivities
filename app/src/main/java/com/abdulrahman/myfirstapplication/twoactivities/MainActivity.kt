package com.abdulrahman.myfirstapplication.twoactivities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var mMessageEditText: EditText
    private lateinit var mReplyHeadTextView:TextView
    private lateinit var mReplyTextView:TextView

    companion object {         // Class name for Log tag
        private val LOG_TAG = MainActivity::class.java.simpleName
        const val EXTRA_MESSAGE="com.abdulrahman.myfirstapplication.twoactivities.extra.MESSAGE"
        const val TEXT_REQUEST=1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mMessageEditText=findViewById(R.id.editText_main)
        mReplyHeadTextView=findViewById(R.id.text_header_reply)
        mReplyTextView =findViewById(R.id.text_message_reply)
        Log.d(LOG_TAG,"--------")
        Log.d(LOG_TAG,"onCreate")
        if(savedInstanceState!=null){
            mReplyHeadTextView.visibility=View.VISIBLE
            mReplyTextView.text=savedInstanceState.getString("reply_text")
            mReplyTextView.visibility=View.VISIBLE

        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (mReplyHeadTextView.visibility==View.VISIBLE){
            outState.putBoolean("reply_visible",true)
            outState.putString("reply_text",mReplyTextView.text.toString())

        }
    }
    override fun onStart() {
        super.onStart()
        Log.d(LOG_TAG,"OnStart")
    }

    override fun onPause() {
        super.onPause()
        Log.d(LOG_TAG,"OnPause")

    }

    override fun onRestart() {
        super.onRestart()
        Log.d(LOG_TAG,"OnReStart")

    }

    override fun onResume() {
        super.onResume()
        Log.d(LOG_TAG,"OnResume")

    }

    override fun onStop() {
        super.onStop()
        Log.d(LOG_TAG,"OnStop")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(LOG_TAG,"OnDestroy")

    }
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode== TEXT_REQUEST){
            if(resultCode== Activity.RESULT_OK){
                var reply = data!!.getStringExtra(SecondActivity.EXTRA_REPLY)
                mReplyHeadTextView.visibility=View.VISIBLE
                mReplyTextView.text=reply
                mReplyTextView.visibility=View.VISIBLE
            }
        }
    }
    fun launchSecondActivity(view: View) {
        val intent=Intent(this,SecondActivity::class.java)
        val message = mMessageEditText.text.toString()
        intent.putExtra(EXTRA_MESSAGE, message);//add the extra

        Log.d(LOG_TAG, "Button clicked!");
        startActivityForResult(intent,TEXT_REQUEST) // for result mens it it takes a message


    }
}
