package com.abdulrahman.myfirstapplication.twoactivities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView

class SecondActivity : AppCompatActivity() {
    companion object{
        const val EXTRA_REPLY="com.abdulrahman.myfirstapplication.twoactivities.extra.REPLY"
    }
    private lateinit var mReply:EditText
    private val LOG_TAG = SecondActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val intent=intent
        val message=intent.getStringExtra(MainActivity.EXTRA_MESSAGE)
        val textView=findViewById<TextView>(R.id.text_message)
        mReply = findViewById(R.id.editText_second)

        textView.text=message
        Log.d(LOG_TAG,"--------")
        Log.d(LOG_TAG,"onCreate")
    }

    override fun onStart() {
        super.onStart()
        Log.d(LOG_TAG,"OnStart")
    }

    override fun onPause() {
        super.onPause()
        Log.d(LOG_TAG,"OnPause")

    }

    override fun onRestart() {
        super.onRestart()
        Log.d(LOG_TAG,"OnReStart")

    }

    override fun onResume() {
        super.onResume()
        Log.d(LOG_TAG,"OnResume")

    }

    override fun onStop() {
        super.onStop()
        Log.d(LOG_TAG,"OnStop")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(LOG_TAG,"OnDestroy")

    }

    fun returnReply(view: View) {
        val reply =mReply.text.toString()
        val replayIntent = Intent()
        replayIntent.putExtra(EXTRA_REPLY,reply)
        setResult(Activity.RESULT_OK,replayIntent)
        finish();



    }
}
